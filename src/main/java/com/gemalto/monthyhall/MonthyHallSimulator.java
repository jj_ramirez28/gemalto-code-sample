package com.gemalto.monthyhall;

import java.util.ArrayList;

public class MonthyHallSimulator {

    private static int timesWon;

    /**
     * Start a new game
     *
     * @param game Game abstraction with all the dynamic options for the game
     */
    private static void playTheGame(final Game game, final Player player) {

        if (game == null || player == null || game.getNumberOfDoors() < 2 || player.getStrategy() == null) {
            System.out.println("Invalid Game configuration, cannot play game");
            return;
        }

        System.out.println("Lets Play!");
        // Change the door with the price
        game.shiftDoors();
        // Player takes first
        player.selectDoor(game.getDoors());
        // Game host shows a door with a Goat
        game.openGoatDoor(player);
        // Player decides to either change the door or stay with the same
        player.makeDecision(game.getDoors(), game.getOpenedGoatDoor());
        boolean playerWon = openDoor(game, player);

        if (playerWon) {
            timesWon++;
            System.out.println("Player Won the Car!");
        } else {
            System.out.println("Player Loose!");
        }
        System.out.println();
    }

    /**
     * Did player won?
     *
     * @param game   Game with the door reference
     * @param player The player with his/her selected door
     * @return true if the selected door has the price
     */
    private static boolean openDoor(final Game game, final Player player) {
        return game.getDoors().get(player.getSelectedDoor()) == 1;
    }

    public static void main(String[] args) {

        Game game;
        Player player;
        int totalGames = 2000;
        int numberOfDoors = 3;

        // Play games with CHANGE strategy
        for (int i = 0; i < totalGames; i++) {
            game = new Game(numberOfDoors, 0, 0, new ArrayList<>());
            player = new Player(0, Strategy.CHANGE);
            MonthyHallSimulator.playTheGame(game, player);
        }
        double percentageWonChange = (double) timesWon / (double) totalGames;

        // Play games with STAY strategy
        timesWon = 0;
        for (int i = 0; i < totalGames; i++) {
            game = new Game(numberOfDoors, 0, 0, new ArrayList<>());
            player = new Player(0, Strategy.STAY);
            MonthyHallSimulator.playTheGame(game, player);
        }
        double percentageWonStay = (double) timesWon / (double) totalGames;

        System.out.println("[[Number of Games: " + totalGames + ", Times Won : " + timesWon + "]]");
        System.out.println("Strategy: " + Strategy.CHANGE);
        System.out.println("Number of Doors: " + numberOfDoors);
        System.out.printf("\n%.2f%s of Games Won with Strategy: %s", percentageWonChange * 100, "%", Strategy.CHANGE);
        System.out.println("\n\n#####\n");

        System.out.println("Number of Games: " + totalGames);
        System.out.println("Strategy: " + Strategy.STAY);
        System.out.println("Number of Doors: " + numberOfDoors);
        System.out.printf("\n%.2f%s of Games Won with Strategy: %s", percentageWonStay * 100, "%", Strategy.STAY);
        System.out.println("\n\n#####\n");
    }


}