package com.gemalto.monthyhall;

/**
 * Strategy for playing Monthy Hall game (Change the door when asked or stay with the same selected door)
 */
public enum Strategy {
    CHANGE, STAY
}