package com.gemalto.monthyhall;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Data
@AllArgsConstructor
class Player {

    private int selectedDoor;
    private Strategy strategy;

    /**
     * Player selects a door
     *
     * @param doors - list of doors for current game
     */
    void selectDoor(final List<Integer> doors) {

        selectedDoor = ThreadLocalRandom.current().nextInt(0, doors.size());
        System.out.println("Doors: " + doors.toString() + ", Player selected Door # " + (selectedDoor + 1));
    }


    /**
     * Gets a door at random like a player guessing for a door
     *
     * @param doors    - list of doors
     * @param goatDoor - Door already opened with a Goat inside
     * @return randomly choosen door which is neither the previously selected door nor the one with the goat
     */
    private int selectDifferentDoor(final List<Integer> doors, final int goatDoor) {

        int newDoor;
        do {
            newDoor = ThreadLocalRandom.current().nextInt(0, doors.size());
        } while (newDoor == selectedDoor || newDoor == goatDoor);
        System.out.println("Player selected door # " + (newDoor + 1));
        return newDoor;
    }

    /**
     * User is asked to make a decision, based on strategy it chooses a new random door (not the same) or stays with the same door
     *
     * @param doors list of doors to select
     */
    void makeDecision(final List<Integer> doors, final int goatDoor) {

        switch (strategy) {
            case CHANGE:
                selectedDoor = selectDifferentDoor(doors, goatDoor);
                System.out.println("Player choose to select a new door, selected # " + selectedDoor);
                break;
            case STAY:
            default:

                System.out.println("Player choose to stay with the same door # " + selectedDoor);
                break;
        }
    }
}
