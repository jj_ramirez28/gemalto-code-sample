package com.gemalto.monthyhall;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

@Data
@AllArgsConstructor
class Game {

    private int numberOfDoors;

    private int pricePosition;

    private int openedGoatDoor;

    private ArrayList<Integer> doors;

    /**
     * Create 1 to N Doors with just one being the one with a price (Default is 3)
     */
    void shiftDoors() {

        for (int i = 0; i < numberOfDoors; i++) {
            doors.add(-1);
        }
        pricePosition = ThreadLocalRandom.current().nextInt(0, numberOfDoors - 1);
        // Add the door with the price
        doors.set(pricePosition, 1);
        System.out.println("Price its hidden behind door # " + (pricePosition + 1));
    }

    /**
     * Open Door with a Goat (Not the price)
     */
    void openGoatDoor(final Player player) {

        do {
            openedGoatDoor = ThreadLocalRandom.current().nextInt(0, numberOfDoors);
        } while (openedGoatDoor == player.getSelectedDoor() || openedGoatDoor == pricePosition);

        System.out.println("Door # " + (pricePosition + 1) + "is opened with a Goat inside");
    }

}