# gemalto-code-sample

Gemalto Code Sample

About sample
======

Monty Hall Problem is a probability puzzle, based on an old TV show, where the host lets a player choose 1 of 3 doors,
one has a car as a prize and the other 2 have goats, the player chooses one, and before open it, the host opens one 
that has the goat on it, then gives the player a second change to either change the door or stay with the same.

This problem solution demostrates (as Steve Selvin did) that its always better to change the selected door after 
the host shows a door with a goat, and in fact after doing the experiment several times the changes to win are twice 
compared with staying with the same door. 
